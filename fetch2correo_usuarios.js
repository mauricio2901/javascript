"use strict";

var div_usuarios = document.querySelector("#usuarios"); // variable para enviar los datos al html
var div_datos = document.querySelector("#usuarios2");
var usuarios2 = [];
var usuarios = [];
fetch("https://reqres.in/api/users")
  .then((data) => data.json())
  .then((users) => {
    usuarios = users.data;

    console.log(usuarios);

    usuarios.map((user, i) => {
      // lo usamos para enviar al html
      let nombre = document.createElement("h2");
      nombre.innerHTML = i + user.first_name + "" + user.last_name;
      div_usuarios.appendChild(nombre); // enviarlos al html
      
    });
  });

fetch("https://reqres.in/api/users")
  .then((data) => data.json())
  .then((user1) => {
    usuarios2 = user1.data;
    console.log(usuarios2);

    usuarios2.map((_user, _i) => {
      let dat = document.createElement("h2");
      dat.innerHTML = _i +" " + _user.email + " " + _user.avatar;
      div_datos.appendChild(dat);

      document.querySelector(".loading").style.display = "none";
    });
  });


//-----------------------------------------------------------------------------

// peticiones ajax con fetch y carga de imagen en html 

"use strict";

var div_usuarios = document.querySelector("#usuarios"); // variable para enviar los datos al html
var div_datos = document.querySelector("#usuarios2");
var div_janet = document.querySelector("#janet");
var usuarios2 = [];

getusuarios()
  .then((data) => data.json())
  .then((users) => {
    listado_usuarios(users.data);
    return getjanet();
  })
  .then((data) => data.json())
  .then((user) => {
    mostrar_janet(user.data);
  });

function getusuarios() {
  return fetch("https://reqres.in/api/users");
}

function getjanet() {
  return fetch("https://reqres.in/api/users/4");
}

function listado_usuarios(usuarios) {
  usuarios.map((user, i) => {
    // lo usamos para enviar al html
    let nombre = document.createElement("h2");
    nombre.innerHTML = i + user.first_name + "" + user.last_name;
    div_usuarios.appendChild(nombre); // enviarlos al html
  });
}

function mostrar_janet(user) {
  // lo usamos para enviar al html
  console.log(user);
  let nombre = document.createElement("h2");
  let avatar = document.createElement("img"); //creamos variable para cargar imagen que en el formato jsoon esta como avatar
  nombre.innerHTML = user.first_name + "" + user.last_name;
  avatar.src = user.avatar; //seleccionamos la imagen
  avatar.width = "100";

  div_janet.appendChild(nombre); // enviarlos al html
  div_janet.appendChild(avatar);
  document.querySelector("loading").style.display = "none";
}


//-------------------------------------------------------------------

// archivo con promesa creada desde cero y peticiones ajax funcionando con fetch


"use strict";

var div_usuarios = document.querySelector("#usuarios"); // variable para enviar los datos al html
var div_datos = document.querySelector("#usuarios2");
var div_janet = document.querySelector("#janet");
var usuarios2 = [];

getusuarios()
  .then((data) => data.json())
  .then((users) => {
    listado_usuarios(users.data);
    return get_info();
  })
  .then((data) => {
    console.log(data);
    return getjanet();
  })
  .then((data) => data.json())
  .then((user) => {
    mostrar_janet(user.data);
  });
function getusuarios() {
  return fetch("https://reqres.in/api/users");
}
function getjanet() {
  return fetch("https://reqres.in/api/users/4");
};

function get_info() {
  var profesor = {
    nombre: "ivan",
    apellido: "Insuasty",
  };

  return new Promise((resolve, reject) => {
    var profesor_string = JSON.stringify(profesor);
    if (typeof profesor_string != "string") return reject("error");
    return resolve(profesor_string);
  });
}

function listado_usuarios(usuarios) {
  usuarios.map((user, i) => {
    // lo usamos para enviar al html
    let nombre = document.createElement("h2");
    nombre.innerHTML = i + user.first_name + "" + user.last_name;
    div_usuarios.appendChild(nombre); // enviarlos al html
  });
}

function mostrar_janet(user) {
  // lo usamos para enviar al html
  console.log(user);
  let nombre = document.createElement("h2");
  let avatar = document.createElement("img"); //creamos variable para cargar imagen que en el formato jsoon esta como avatar
  nombre.innerHTML = user.first_name + "" + user.last_name;
  avatar.src = user.avatar; //seleccionamos la imagen
  avatar.width = "100";

  div_janet.appendChild(nombre); // enviarlos al html
  div_janet.appendChild(avatar);
  document.querySelector("loading").style.display = "none";
}



