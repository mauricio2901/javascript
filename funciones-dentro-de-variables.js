"use strict";

var car = {
  marca: "crysler ",
  modelo: 1998,
  cilindraje: 3600,

  mostrar() {
    console.log(this.marca, this.modelo, this.cilindraje);
  },
};

document.write("<h1>" + car.marca + "</h1>");
car.mostrar();
console.log(car);
